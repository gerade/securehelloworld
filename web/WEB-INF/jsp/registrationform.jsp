<%@ page session="true"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="js/passwordstrength.js" type="text/javascript"
	language="javascript"></script>

<title>Registration Form</title>

</head>

<body>
	<h1 align="center">Registration Form</h1>
	<p>
		Registration password must be strong and contain at least 1 number, at
		least 1 lower case letter, and at least 1 upper case letter and at
		least one special character. Minimal password length is 8 characters.

		<p>
<form:form action="registrationform.html" method="post"
			commandName="registration">

			<table cellspacing="5">
				<tr>
					<td width="150"><form:label path="userName">Name:</form:label>
					</td>
					<td width="200"><form:input path="userName" maxlength="50"
							size="30" /></td>
					<td width="300"><form:errors path="userName"
							cssStyle="color:red" /></td>
				</tr>

				<tr>
					<td><form:label path="password">Password:</form:label></td>
					<!--
					Note: password will be stored unencrypted. It should be hashed with the salt and stored. 
					 -->
					<td><form:password path="password" maxlength="30" size="30"
							onkeyup="return passwordChanged();" /></td>
					<td><span id="strength">Strong password is needed to
							proceed with registration.</span> <form:errors path="password"
							cssStyle="color:red" /></td>
				</tr>


				<tr>
					<td><form:label path="confirmPassword">Confirm Password:</form:label>
					</td>
					<td><form:password path="confirmPassword" maxlength="30"
							size="30" /></td>
					<td><form:errors path="confirmPassword" cssStyle="color:red" />
					</td>
				</tr>


				<tr>
					<td colspan="2" align="right"><input type="submit"
						value="  Register  " /></td>
				</tr>
			</table>

		</form:form>
	</p></body>

</html>