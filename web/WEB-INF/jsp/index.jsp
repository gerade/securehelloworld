<%@ page session="true"%>
<%@ taglib prefix='security' uri='http://www.springframework.org/security/tags' %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

<head>
<title>Hello World!</title>
</head>

<body>
<h1 align="center">Hello World!</h1>

<span id="loginstatus">You are logged in as <security:authentication property="principal.username"/>
</span>

<ul><li><a href="logout.html">Logout</a></li></ul>
</body>

</html>
