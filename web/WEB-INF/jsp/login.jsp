<%@ page session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

<head>
<title>Login: Spring Security Web Application</title>
</head>



<body onload='document.loginForm.j_username.focus();'>

	<form id="loginForm" name="loginForm" action="j_spring_security_check"
		method="post">
		<c:if test="${not empty param.authfailed}">
			<span id="infomessage" style="color: red;"> Login failed due
				to: <c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}" />.
				Please try again or register new account. </span>
		</c:if>
		<c:if test="${not empty param.loggedout}">
			<span id="infomessage" style="color: green;"> You have been
				successfully logged out. </span>
		</c:if>
		<table>
			<tr>
				<td>Username</td>
				<td><input id="usernameField" type="text" name="j_username"
					value="<c:out value="${SPRING_SECURITY_LAST_USERNAME}"/>" /></td>
			</tr>
			<tr>
				<td>Password</td>
				<td><input id="passwordField" type="password" name="j_password" />
				</td>
			</tr>

			<tr>
				<td colspan="2" align="right"><input type="submit"
					value="Login" /></td>
			</tr>
		</table>
	</form>

	<ul>
		<li><a href="registrationform.html">Registration Form</a></li>
	</ul>

</body>

</html>