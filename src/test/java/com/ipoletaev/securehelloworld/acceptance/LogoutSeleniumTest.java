package com.ipoletaev.securehelloworld.acceptance;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;


public class LogoutSeleniumTest {
	private static final String WELCOME_URL = "http://localhost:8080/securehelloworld/index.html";
	private static final String ROOT_URL = "http://localhost:8080/securehelloworld/";
	
    private final WebDriver firefoxDriver = new FirefoxDriver();
//
//	@Test
//    public void shouldBeAbleToLogoutOfApplicationAndSeeMessageThatConfirmsThis() {
//
//        // try to access admin
//		firefoxDriver.get("http://localhost:8080/springsecuritywebapp/home.htm");
//
//        login(firefoxDriver);
//
//        // verify
//        final WebElement logoutLink = firefoxDriver.findElement(By.linkText("Logout"));
//        logoutLink.click();
//
//        assertThat(firefoxDriver.getTitle(),
//                is("Login: Spring Security Web Application"));
//
//        final WebElement informationMessageSection = firefoxDriver.findElement(By
//                .id("infomessage"));
//        assertThat(informationMessageSection.getText(),
//                containsString("You have been successfully logged out."));
//    }
}
