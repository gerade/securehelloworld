package com.ipoletaev.securehelloworld.acceptance;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.containsString;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * I contain end-to-end acceptance/functional tests for the simple web
 * application with spring security.
 *
 * I specifically contain tests that verify the behaviour of UserStory 2.
 */
public class FunctionalRequirement4AcceptanceSeleniumTest {
	
	private static final String WELCOME_URL = "http://localhost:8080/securehelloworld/index.html";
	private static final String ROOT_URL = "http://localhost:8080/securehelloworld/";
	
    private final WebDriver firefoxDriver = new FirefoxDriver();

    @After
    public void aTearDownMethodForEachTest() {
        firefoxDriver.close();
    }

    @Test
    public void shouldBeAbleToViewHomePageWhenSuccessfullyAuthenticated() {

        // try to get to home.htm
        firefoxDriver.get(WELCOME_URL);

        // check we are on springs login page
        assertThat(firefoxDriver.getTitle(), is("Login Page"));

        final WebElement usernameField = firefoxDriver.findElement(By
                .name("j_username"));
        usernameField.sendKeys("username");

        final WebElement passwordField = firefoxDriver.findElement(By
                .name("j_password"));
        passwordField.sendKeys("password");

        passwordField.submit();

        // state verification
        assertThat(firefoxDriver.getTitle(),
                is("Home: Spring Security Web Application"));
    }
    
	@Test
	public void shouldBeTakenToHomePageWhenVisitingApplicationRootURLAndAlreadyAuthenticated() {

		firefoxDriver.get(WELCOME_URL);

		// login
		final WebElement usernameField = firefoxDriver.findElement(By
				.name("j_username"));
		usernameField.sendKeys("username");

		final WebElement passwordField = firefoxDriver.findElement(By
				.name("j_password"));
		passwordField.sendKeys("password");

		passwordField.submit();

		// goto application root url
		firefoxDriver.get(ROOT_URL);

		// state verification
		assertThat(firefoxDriver.getTitle(),
				is("Home: Spring Security Web Application"));
	}
    
    @Test
	public void shouldBeTakenToLoginPageWhenVisitingApplicationRootURLAndNotAuthenticated() {

		firefoxDriver.get(ROOT_URL);

		// state verification
		assertThat(firefoxDriver.getTitle(),
				is("Login: Spring Security Web Application"));
	}
    
    @Test
    public void shouldRemainOnLoginPageWithInformativeMessageWhenAuthenticationFailsDueToIncorrectPassword() {

        // try to get to home.htm
    	firefoxDriver.get("http://localhost:8080/springsecuritywebapp/login.jsp");

        final WebElement usernameField = firefoxDriver.findElement(By
                .name("j_username"));
        usernameField.sendKeys("username_does_not_exist");

        final WebElement passwordField = firefoxDriver.findElement(By
                .name("j_password"));
        passwordField.sendKeys("password");

        passwordField.submit();

        // state verification
        assertThat(firefoxDriver.getTitle(),
                is("Login: Spring Security Web Application"));

        final WebElement informationMessageSection = firefoxDriver.findElement(By
                .id("infomessage"));
        assertThat(informationMessageSection.getText(),
                containsString("Login failed due to: Bad credentials."));
    }

    @Test
    public void shouldRemainOnLoginPageWithInformativeMessageWhenAuthenticationFailsDueToIncorrectUsername() {

        // try to get to home.htm
    	firefoxDriver.get("http://localhost:8080/springsecuritywebapp/login.jsp");

        final WebElement usernameField = firefoxDriver.findElement(By
                .name("j_username"));
        usernameField.sendKeys("username_does_not_exist");

        final WebElement passwordField = firefoxDriver.findElement(By
                .name("j_password"));
        passwordField.sendKeys("password");

        passwordField.submit();

        // state verification
        assertThat(firefoxDriver.getTitle(),
                is("Login: Spring Security Web Application"));

        final WebElement informationMessageSection = firefoxDriver.findElement(By
                .id("infomessage"));
        assertThat(informationMessageSection.getText(),
                containsString("Login failed due to: Bad credentials."));
    }

    @Test
    public void shouldRemainOnLoginPageWithUsernameStillPopulatedWhenAuthenticationFails() {

        // try to get to home.htm
    	firefoxDriver.get("http://localhost:8080/springsecuritywebapp/login.jsp");

        final WebElement usernameField = firefoxDriver.findElement(By
                .name("j_username"));
        usernameField.sendKeys("username_does_not_exist");

        final WebElement passwordField = firefoxDriver.findElement(By
                .name("j_password"));
        passwordField.sendKeys("password");

        passwordField.submit();

        // state verification
        assertThat(firefoxDriver.getTitle(),
                is("Login: Spring Security Web Application"));

        final WebElement username = firefoxDriver.findElement(By.name("j_username"));

        assertThat(username.getText(), is("username_does_not_exist"));
    }
}