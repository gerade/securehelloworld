package com.ipoletaev.securehelloworld.acceptance;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.opera.core.systems.OperaDriver;

/**
 * Test functional requirement 5:
 * <p>
 * - Only authenticated users may view the �Hello World!� dummy page
 * <p>
 * on different browsers using Selenium framework.
 * 
 */
public class FunctionalRequirement5AcceptanceSeleniumTest {

	private static final String WELCOME_URL = "http://localhost:8080/securehelloworld/index.html";

	private final WebDriver firefoxDriver = new FirefoxDriver();
	private final WebDriver internetExplorerDriver = new InternetExplorerDriver();
	private final WebDriver chromeDriver = new ChromeDriver();
	private final WebDriver operaDriver = new OperaDriver();

	@After
	public void aTearDownMethodForEachTest() {
		// close firefox browser that was launch after each test
		firefoxDriver.close();
		internetExplorerDriver.close();
		chromeDriver.close();
		operaDriver.close();
	}

	/**
	 * Test requirement on Firefox browser.
	 */
	@Test
	public void checkWelcomeFileTitleWhenNotAuthentificatedOnFirefox() {

		firefoxDriver.get(WELCOME_URL);
		assertThat(firefoxDriver.getTitle(), is(not("Hello World!")));
		assertThat(firefoxDriver.getTitle(), is("Authorization requered."));
	}

	/**
	 * Test requirement on Internet Explorer browser.
	 */

	@Test
	public void checkWelcomeFileTitleWhenNotAuthentificatedOnInternetExplorer() {

		internetExplorerDriver.get(WELCOME_URL);

		assertThat(internetExplorerDriver.getTitle(), is(not("Hello World!")));
		assertThat(internetExplorerDriver.getTitle(),
				is("Authorization requered."));
	}

	/**
	 * Test requirement on Google Chrome browser.
	 */

	@Test
	public void checkWelcomeFileTitleWhenNotAuthentificatedOnChrome() {

		chromeDriver.get(WELCOME_URL);
		assertThat(chromeDriver.getTitle(), is(not("Hello World!")));
		assertThat(chromeDriver.getTitle(), is("Authorization requered."));
	}

	/**
	 * Test requirement on Opera browser.
	 */

	@Test
	public void checkWelcomeFileTitleWhenNotAuthentificatedOnOpera() {

		operaDriver.get(WELCOME_URL);
		assertThat(operaDriver.getTitle(), is(not("Hello World!")));
		assertThat(operaDriver.getTitle(), is("Authorization requered."));
	}
}