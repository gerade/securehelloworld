package com.ipoletaev.securehelloworld.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ipoletaev.securehelloworld.dao.JdbcAuthorityDao;
import com.ipoletaev.securehelloworld.dao.JdbcUserDao;
import com.ipoletaev.securehelloworld.model.Authority;
import com.ipoletaev.securehelloworld.model.User;

/**
 * Registration service implementation.
 * 
 * Note: password stored unencrypted. It should be hashed on client side after
 * adding a salt.
 * 
 * @author Igor Poletaev
 * 
 */
@Service
public class UserRegistrationService {
	public static final String DEFAULT_ROLE = "ROLE_USER";
	@Autowired
	private JdbcAuthorityDao authorityDao;
	@Autowired
	private JdbcUserDao userDao;

	public void setUserDao(JdbcUserDao jdbcUserDao) {
		this.userDao = jdbcUserDao;
	}

	public void setAuthorityDao(JdbcAuthorityDao jdbcAuthorityDao) {
		this.authorityDao = jdbcAuthorityDao;
	}

	public void registerUser(String username, String password) {
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setEnabled(true);
		userDao.saveUser(user);

		Authority authority = new Authority();
		authority.setUsername(username);
		authority.setAuthority(DEFAULT_ROLE);
		authorityDao.saveAuthority(authority);

	}

	public boolean checkIfUserExists(String userName) {
		return userDao.checkPresence(userName);
	}

}
