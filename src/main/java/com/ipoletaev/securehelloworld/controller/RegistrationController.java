package com.ipoletaev.securehelloworld.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ipoletaev.securehelloworld.model.Registration;
import com.ipoletaev.securehelloworld.service.UserRegistrationService;

@Controller
@RequestMapping("/registrationform.html")
public class RegistrationController {
	@Autowired
	private RegistrationValidation registrationValidation;
	@Autowired
	private UserRegistrationService userRegistrationService;

	public void setRegistrationValidation(UserRegistrationService service) {
		this.userRegistrationService = service;
	}

	public void setUserRegistrationService(
			RegistrationValidation registrationValidation) {
		this.registrationValidation = registrationValidation;
	}

	// Display the form on the get request
	@RequestMapping(method = RequestMethod.GET)
	public String get(final ModelMap model) {
		Registration registration = new Registration();
		model.addAttribute("registration", registration);
		return "registrationform";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String post(final ModelMap model,
			@Valid final Registration registration, final BindingResult result) {

		registrationValidation.validate(registration, result);
		if (!result.hasErrors()) {

			boolean isUserExists = userRegistrationService
					.checkIfUserExists(registration.getUserName());
			if (!isUserExists) {
				userRegistrationService.registerUser(
						registration.getUserName(), registration.getPassword());
				return "registrationsuccess";
			}

			result.rejectValue("userName",
					"userExists.registration.userName",
					"User with specified username already exists.");
		}
		return "registrationform";
	}
}
