package com.ipoletaev.securehelloworld.dao;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.ipoletaev.securehelloworld.model.Authority;

/**
 * Authority DAO implementation.
 * 
 * @author Igor Poletaev
 * 
 */
@Component("authorityDao")
public class JdbcAuthorityDao implements AuthorityDao {
	private final Log logger = LogFactory.getLog(getClass());
	private static final String INSERT_AUTHORITY_QUERY = "INSERT INTO AUTHORITIES VALUES(?,?)";
	
	private JdbcTemplate jdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

	@Override
	public void saveAuthority(Authority authority) {
		logger.info("Saving authority for user: " + authority.getUsername());
		int count = jdbcTemplate.update(
				INSERT_AUTHORITY_QUERY,
				new Object[] { authority.getUsername(),
						authority.getAuthority() });
		logger.info("Rows affected: " + count);
	}

}
