package com.ipoletaev.securehelloworld.dao;

import com.ipoletaev.securehelloworld.model.Authority;

/**
 * DAO interface for user authority object.
 * 
 * @author Igor Poletaev
 * 
 */
public interface AuthorityDao{
	public void saveAuthority(Authority authority);
}
