package com.ipoletaev.securehelloworld.dao;

import com.ipoletaev.securehelloworld.model.User;

/**
 * DAO interface for User object.
 * 
 * @author Igor Poletaevs
 * 
 */
public interface UserDao {

	public boolean checkPresence(String name);

	public void saveUser(User user);
}
