package com.ipoletaev.securehelloworld.dao;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.ipoletaev.securehelloworld.model.User;

/**
 * User DAO implementation. Is used to check if user is already registered and
 * to save user credentials.
 * 
 * @author Igor Poletaev
 * 
 */
@Component("userDao")
public class JdbcUserDao implements UserDao {
	private final Log logger = LogFactory.getLog(getClass());
	private static final String COUNT_USERS_WITH_NAME_QUERY = "SELECT COUNT(*) FROM USERS WHERE USERNAME = ?";
	private static final String INSERT_USER_QUERY = "INSERT INTO USERS VALUES(?,?,?)";

	private JdbcTemplate jdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
	
	@Override
	public boolean checkPresence(String username) {
		logger.info("Getting count of user with username = " + username);
		Integer countOfUsersWithUsername = jdbcTemplate.queryForInt(
				COUNT_USERS_WITH_NAME_QUERY, new Object[] { username });
		return countOfUsersWithUsername > 0;
	}

	@Override
	public void saveUser(User user) {
		logger.info("Saving user: " + user.getUsername());
		int count = jdbcTemplate.update(INSERT_USER_QUERY, new Object[] {
				user.getUsername(), user.getPassword(),user.getEnabled() });
		logger.info("Rows affected: " + count);
	}
}
