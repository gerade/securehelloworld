package com.ipoletaev.securehelloworld.model;

/**
 * Represents user credentials.
 * 
 * @author Igor Poletaev
 * 
 */
public class User {
	private String username;
	private String password;
	private Boolean enabled;

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
