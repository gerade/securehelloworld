package com.ipoletaev.securehelloworld.model;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.SafeHtml;

/**
 * Registration form bean.
 * 
 * Username is checked to contain only SafeHtml. Strong regexp
 * ((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{8,30}) is used to validate
 * password strength. Same regexp is used to validate password on client
 * side using JS.
 * 
 * @author Igor Poletaev
 * 
 */
public class Registration {
	private static final String STRONG_PASSWORD_REGEXP = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{8,30})";
	@NotEmpty
	@Size(max = 20, message = "{Username should be less than 20 characters}")
	@SafeHtml
	private String userName;
	@NotEmpty
	@Size(min = 8, max = 30)
	@Pattern(regexp = STRONG_PASSWORD_REGEXP, message = "{Password is too weak. It should contain Characters numbers and symbols}")
	private String password;
	@NotEmpty
	@Size(min = 8, max = 30)
	@Pattern(regexp = STRONG_PASSWORD_REGEXP, message = "{Password is too weak. It should contain Characters numbers and symbols}")
	private String confirmPassword;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

}
